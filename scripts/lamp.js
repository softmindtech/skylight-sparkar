/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */
const Diagnostics = require('Diagnostics');
const Materials = require('Materials')
const Textures = require('Textures')
const Shaders = require('Shaders')
const R = require('Reactive')
const {font} = require('./font.js');
const tex = Textures.get('font');
const emojiTex = {
    angry: Textures.get('angry'),
    wow: Textures.get('wow'),
    like: Textures.get('like'),
    haha: Textures.get('haha'),
    sad: Textures.get('sad'),
    love: Textures.get('love')
}

const assignEmoji = (mat, input) => {
    // print bounding box
    // center, middle align
    const printX = 0.45;
    const printWidth = 0.15;
    const printY = 0.69;
    const printHeight = 0.20;
    const printStartX = printX - printWidth / 2;
    const printStartY = printY - printHeight / 2;

    const uv = Shaders.fragmentStage(Shaders.vertexAttribute({ variableName: Shaders.VertexAttribute.TEX_COORDS }))
    // let newUV = R.pack2(0, 0);


    let cond1 = R.step(uv.x, printStartX);
    let cond2 = R.sub(1, R.step(uv.x, R.add(printStartX, printWidth)));
    let cond3 = R.step(uv.y, printStartY);
    let cond4 = R.sub(1, R.step(uv.y, R.add(printStartY, printHeight)));
    let cond = R.mul(R.mul(R.mul(cond1, cond2), cond3), cond4);

    let x = R.div( R.sub(uv.x, printStartX), printWidth);
    let y = R.div( R.sub(uv.y, printStartY), printHeight);
    // x = R.add(R.mul(x, 1), 0);
    // y = R.add(R.mul(y, 1), 0);
    
    x = R.mul(cond, x);
    y = R.mul(cond, y);

    // newUV = R.add(newUV, R.pack2(x, y));  
    let newUV = R.pack2(x, y);

    // const alphaTex = R.pack4(angry.signal.x, angry.signal.y, angry.signal.z, angry.signal.w);
    const finalColor = Shaders.textureSampler(emojiTex[input].signal, newUV );
    mat.setTextureSlot(Shaders.DefaultMaterialTextures.DIFFUSE, finalColor);
}

const assignMaterial = (mat, input) => {
    const uv = Shaders.fragmentStage(Shaders.vertexAttribute({ variableName: Shaders.VertexAttribute.TEX_COORDS }))
    const lines = input.split("\n");

    // print bounding box
    // center, middle align
    const printX = 0.45;
    const printWidth = 0.15;
    const printY = 0.69;
    const printHeight = 0.30;

    let lineOriginalWidths = [];
    let originalWidth = 0;
    for (let l = 0; l < lines.length; l++) {
        let lineOriginalWidth = 0;
        for (let i = 0; i < lines[l].length; i++) {
            let c = font.characters[lines[l].charAt(i)];
            if (!c) continue;

            lineOriginalWidth += c.advance;
        }
        lineOriginalWidths.push(lineOriginalWidth);
        originalWidth = Math.max(originalWidth, lineOriginalWidth);
    }
    let originalHeight = font.size * lines.length;

    let ratioWidth = printWidth / originalWidth;
    let ratioHeight = printHeight / originalHeight;
    let ratio = Math.min(ratioWidth, ratioHeight);
    let totalHeight = originalHeight * ratio;

    let positions = [];
    let curY = printY + - totalHeight / 2 + font.size * ratio;

    for (let l = 0; l < lines.length; l++) {
        let curX = printX - lineOriginalWidths[l] * ratio / 2;
        for (let i = 0; i < lines[l].length; i++) {
            let c = font.characters[lines[l].charAt(i)];
            if (!c) continue;

            positions.push({
                from: {
                    x: curX - c.originX * ratio,
                    y: curY - c.originY * ratio,
                    h: c.height * ratio,
                    w: c.width * ratio
                },
                to: {
                    x: c.x / font.width,
                    y: c.y / font.height,
                    w: c.width / font.width,
                    h: c.height / font.height,
                }
            });
            curX += c.advance * ratio;
        }
        curY += font.size * ratio;
    }

    let newUV = R.pack2(0, 0);
    for (let i = 0; i < positions.length; i++) {
        let position = positions[i];
        
        let cond1 = R.step(uv.x, position.from.x);
        let cond2 = R.sub(1, R.step(uv.x, R.add(position.from.x, position.from.w)));
        let cond3 = R.step(uv.y, position.from.y);
        let cond4 = R.sub(1, R.step(uv.y, R.add(position.from.y, position.from.h)));
        let cond = R.mul(R.mul(R.mul(cond1, cond2), cond3), cond4);

        let x = R.div( R.sub(uv.x, position.from.x), position.from.w);
        let y = R.div( R.sub(uv.y, position.from.y), position.from.h);
        x = R.add(R.mul(x, position.to.w), position.to.x);
        y = R.add(R.mul(y, position.to.h), position.to.y);
        
        x = R.mul(cond, x);
        y = R.mul(cond, y);

        newUV = R.add(newUV, R.pack2(x, y));    
    }

    // const alphaTex = R.pack4(tex.signal.x, tex.signal.y, tex.signal.z, R.step(tex.signal.x, 0.1));
    const alphaTex = R.pack4( R.sub(1, tex.signal.x), R.sub(1, tex.signal.y), R.sub(1, tex.signal.z), R.step(tex.signal.x, 0.1));
    const finalColor = Shaders.textureSampler(alphaTex, newUV );
    // const finalColor = R.pack4(1.0, 1.0, 0.0, 0.1);
    // mat.setTextureSlot(Shaders.DefaultMaterialTextures.DIFFUSE, finalColor);
    mat.setTextureSlot(Shaders.DefaultMaterialTextures.DIFFUSE, finalColor);
    // mat.setTextureSlot(Shaders.DefaultMaterialTextures.EMISSIVE, R.pack4(0.89, 0.44, 0.03, 0.1));
    // mat.setTextureSlot(Shaders.DefaultMaterialTextures.EMISSIVE, finalColor);
}

export {
    assignMaterial,
    assignEmoji
}