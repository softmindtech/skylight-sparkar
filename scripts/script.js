/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');

const Diagnostics = require('Diagnostics');
const Patches = require('Patches');
const Materials = require('Materials')
const Animation = require('Animation');
const Random = require('Random');
const Time = require('Time');
const Shaders = require('Shaders');
const R = require('Reactive');
const {assignMaterial, assignEmoji} = require('./lamp.js');
// const LiveStreaming = require('LiveStreaming');

var LiveStreaming = {};
var testComments = [
  {body: 'Hello'},
  {body: 'Peace!'}, 
  {body: 'You can do it!'},
  {body: '^.^'},
  {body: 'Love ya!'}, 
  {body: 'Beautiful'},   
  {body: 'ByeBye \n COVID-19'},
  {body: 'NO Wars'},
  {body: 'Humanity'},
]
var reactionTypes = ['haha', 'like', 'angry', 'love', 'sad', 'wow']

let textCommentIndex = 0;
LiveStreaming.comments = {};
LiveStreaming.reactions = {};
LiveStreaming.comments.stream = {
  subscribe : function(delegate) {
    const Time = require('Time');
    const timeInMilliseconds = 2000;
    const mockNewCommentFromLive = () => {
      delegate(testComments[textCommentIndex]);
      textCommentIndex = (textCommentIndex + 1) % testComments.length;
    }
    Time.setInterval(mockNewCommentFromLive, timeInMilliseconds);
  }
}
LiveStreaming.reactions.angry = {};
LiveStreaming.reactions.angry.monitor = () => {
  return {
    subscribe : function(delegate) {
      const Time = require('Time');
      const timeInMilliseconds = 10000;
      const mockNewReactionFromLive = () => {
        delegate(1);
      }
      Time.setInterval(mockNewReactionFromLive, timeInMilliseconds);
    }
  }
}
LiveStreaming.reactions.haha = LiveStreaming.reactions.angry;
LiveStreaming.reactions.like = LiveStreaming.reactions.angry;
LiveStreaming.reactions.wow = LiveStreaming.reactions.angry;
LiveStreaming.reactions.love = LiveStreaming.reactions.angry;
LiveStreaming.reactions.sad = LiveStreaming.reactions.angry;

const nextRender = [];
LiveStreaming.comments.stream.subscribe(function(comment) {
  if (nextRender.length < 30) {
    nextRender.push({
      type: 'text',
      content: comment.body
    });
  }  
});
LiveStreaming.reactions.angry.monitor({fireOnInitialValue: false}).subscribe((e) => {
  if (nextRender.length < 30) nextRender.push({type: 'angry'});
});
LiveStreaming.reactions.wow.monitor({fireOnInitialValue: false}).subscribe((e) => {
  if (nextRender.length < 30) nextRender.push({type: 'wow'});
});
LiveStreaming.reactions.like.monitor({fireOnInitialValue: false}).subscribe((e) => {
  if (nextRender.length < 30) nextRender.push({type: 'like'});
});
LiveStreaming.reactions.haha.monitor({fireOnInitialValue: false}).subscribe((e) => {
  if (nextRender.length < 30) nextRender.push({type: 'haha'});
});
LiveStreaming.reactions.sad.monitor({fireOnInitialValue: false}).subscribe((e) => {
  if (nextRender.length < 30) nextRender.push({type: 'sad'});
});
LiveStreaming.reactions.love.monitor({fireOnInitialValue: false}).subscribe((e) => {
  if (nextRender.length < 30) nextRender.push({type: 'love'});
});



let startTrigger = Patches.getPulseValue('startTrigger');
const spawn = (i) => {
  const object = objects[i];
  const newX = Random.random() * 6 - 3;
  const newZ = Random.random() * 6 - 3;
  object.transform.x = newX;
  object.transform.z = newZ;

  if (nextRender.length > 0) {    
    const r = nextRender.pop();
    if (r.type === 'text') {
      assignMaterial(Materials.get("lamp_paint" + i), r.content);
    } else {
      assignEmoji(Materials.get("lamp_paint" + i), r.type);
    }
  } else {    
    assignMaterial(Materials.get("lamp_paint" + i), '');
  }
}

const OBJECT_COUNT = 21;
const objects = [];
for (var i = 0; i < OBJECT_COUNT; i++) {
    objects.push(Scene.root.find('lamp' + i));
    objects[i].hidden = true;
}

let started = false;
startTrigger.subscribe( (e) => {
  if (started) return;
  started = true;

  for (let i = 0; i < objects.length; i++) {
    const object = objects[i];
    
    const dropDriver = Animation.timeDriver({
      durationMilliseconds: 12000,
      loopCount: Infinity
    });

    dropDriver.onAfterIteration().subscribe((e) => {
      spawn(i);
    })
    Time.setTimeout(() => {
      objects[i].hidden = false;
      spawn(i);
      dropDriver.start();
    }, 1200 * i);

    const dropAnimation = Animation.animate(dropDriver,   Animation.samplers.bezier(0, 1, 2, 20));
    object.transform.y = dropAnimation;
  }
})


// assignMaterial(Materials.get("lamp_paint0"), "abc de#$%!\nZZw\nA\nB\nC\nD\nD\nD");
// assignMaterial(Materials.get("lamp_paint1"), "大Z");